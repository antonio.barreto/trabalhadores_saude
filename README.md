# Trabalhadores da Saúde no Brasil



## Introdução

O repositório se destina a estudos sobre os trabalhadores da saúde no Brasil, a partir de uma perspectiva crítica e de uma abordagem fundamentalmente de economia política. 

Projeto de ciência aberta, replicável, a partir de dados públicos e pesquisa primária projetada.

A parte inicial do projeto é inspirada no [Anuário dos Trabalhadores do SUS](https://www.dieese.org.br/anuario/2018/AnuarioSUS.html), de 2019, produzido por parceria entre o Departamento Intersindical de Estatística e Estudos Sócioeconômicos (DIEESE) e o Ministério da Saúde (MS).

## Fontes de dados e status

- [ ] PNAD Contínua
- [ ] RAIS/CAGED
- [ ] CNES
- [ ] Pesquisa de Orçamentos Familiares
- [ ] World Labour Values Database

## Autores
Antonio Angelo Menezes Barreto - Consultor do Ministério da Saúde, Doutorando em Saúde Pública pela USP


Rodrigo E. S. Borges - Consultor do Ministério da Saúde, Doutor em Economia pela Universidad Complutense de Madrid

## Licença
CC-BY-SA-NC 4.0

## Project status
Fase inicial do projeto.
